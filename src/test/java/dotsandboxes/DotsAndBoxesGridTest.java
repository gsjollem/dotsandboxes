package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    DotsAndBoxesGrid grid = new DotsAndBoxesGrid(15, 8, 1);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.


    @Test
    public void testLineDraw(){

        //This test will draw the lines that will also be used in the second test. It should draw the box
        //then attempt to draw one of the lines again.

        logger.info("Testing duplicate line drawing");

        grid.drawHorizontal(0, 0, 1);
        grid.drawHorizontal(0,1, 1);
        grid.drawVertical(0,0,1);
        grid.drawVertical(1,0,1);

        assertThrows(IndexOutOfBoundsException.class, () -> {  grid.drawHorizontal(0, 0, 1);} );
        //assertFalse(grid.drawHorizontal(0, 0, 1));

    }


    @Test
    public void testBoxComplete()
    {
        // will need to draw a box, then give it to box complete and assert that it should be true

        grid.drawHorizontal(0, 0, 1);
        grid.drawHorizontal(0,1, 1);
        grid.drawVertical(0,0,1);
        grid.drawVertical(1,0,1);

        logger.info("Testing box completion");
        assertTrue(grid.boxComplete(0,0));
        assertFalse(grid.boxComplete(2,2));
    }

}
